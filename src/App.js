import {useState, useEffect} from 'react';
import TodoCard from './TodoCard'
import './App.css';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import {Button, Container} from 'react-bootstrap';


export default function App() {

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [newName, setName] = useState('')
  const [newTime, setTime] = useState('')


  const [lists, setLists] = useState([])
  console.log(lists)

  useEffect(() => {
    fetch('https://peaceful-garden-15029.herokuapp.com/todolist/')
    .then(res => res.json())
    .then(data => {

      setLists(data.map(list =>{
        return(
          <TodoCard key={list._id} listProp = {list}/>
          )
      }))
    })
  }, [])

  const CreateTodo = () => {

    if(newName  === ''){
      alert('Name is required')
    }else if(newTime === ''){
      alert('Time is required')
    }

    
    fetch('https://peaceful-garden-15029.herokuapp.com/todolist/new', {
      method: 'POST',
      headers: {
        "Content-Type" : "application/json"
      },
      body: JSON.stringify({
        name: newName,
        time: newTime
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      window.location.reload()
    })
  }

  const refreshPage = () => {
    window.location.reload();
  }

  const createRefresh = () => {
    handleClose()
    refreshPage()
  }

  function getYear() {
    const date = new Date()
    const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
]
    return `${monthNames[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`;
}


  return(
    <>
    <Container className='p-4 conts'>
    <h1 className='my-3'> To-Do-List for {getYear()}</h1>
    <Button className='bootbut' onClick={handleShow}>Create New</Button>
    <div>

    <Modal show={show} onHide={handleClose} >
        <Modal.Header closeButton>
          <Modal.Title>Update Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                autoFocus
                 onChange={e => setName(e.target.value)}
                 required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Time</Form.Label>
              <Form.Control
                type="time"
                autoFocus
                 onChange={e => setTime(e.target.value)}
                 required
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={createRefresh} >
            Close
          </Button>
          <Button variant="primary" type="submit" id="submitBtn" onClick={e => CreateTodo(e)} >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>


    </div>
    {lists}
    </Container>
    </>
  )
}
