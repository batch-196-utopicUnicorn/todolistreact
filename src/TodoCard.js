import {Card, Button} from 'react-bootstrap';
import {useEffect, useState} from 'react';
import React from 'react'



export default function TodoCard(props){

	const [newName, setName] = useState('')
	const [newTime, setTime] = useState('')
	console.log(newName)



	console.log(props);
	const {name, time, _id} = props.listProp
	console.log(name)

	const updateTodo = (e) => {

		if(newName  === ''){
			alert('Name is required')
		}else if(newTime === ''){
			alert('Time is required')
		}

		 else{
		fetch(`https://peaceful-garden-15029.herokuapp.com/todolist/edit/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				name: newName,
				time: newTime
			})

		})
		.then(res => res.json())
		.then(data => alert("Please refresh the page to show update"))
	}

	}

	const refreshPage = () => {
    window.location.reload();
  }

	const updateRefresh = () => {
    updateTodo()
  
  }	

	
	function deleteTodo(){

		fetch(`https://peaceful-garden-15029.herokuapp.com/todolist/delete/${_id}`, {
			method: 'DELETE',
			headers: {
				'Content-Type' : 'application/json'
			}
		})
		.then(res => res.json())
		.then(data => console.log(data))
	}

	function deleteRefresh(){
		deleteTodo()
		refreshPage()
	}


	// function changeCSS(){
	// const appear = document.querySelectorAll('.inputText')
	// appear.classList += ' inactive'
	// appear.classList -= ' active'
	// }

	// function revert(){
	// 	const appear = document.querySelectorAll('.inputText').style.display ="block";
	// }

	 const hiide = document.querySelectorAll('.inputText')
	 const [isActive, setIsActive] = useState(false);
	 const [isDone, setIsDone] = useState(false)

	const handleClick = event => {
    // 👇️ toggle isActive state on click
    setIsActive(current => !current);
  	};

  	const handleClickDone = event => {
  		setIsDone(current => !current);
  	}

	return(
		<>
		
		<Card className="my-3">
		<Card.Body>
		<Card.Text className={isDone ? 'done' : ''}>{name} at {time} &nbsp;</Card.Text>
		<input type="text" className={isActive ? "active text-center" : 'inactive text-center'} placeholder="Input activity" onChange={e => setName(e.target.value)} />
		<input type="time" className={isActive ? "active" : 'inactive'}  onChange={e => setTime(e.target.value)} />
		<Button  variant="light" onClick={handleClickDone}>
		{
			isDone ? <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-check2-square" viewBox="0 0 16 16">
  <path d="M3 14.5A1.5 1.5 0 0 1 1.5 13V3A1.5 1.5 0 0 1 3 1.5h8a.5.5 0 0 1 0 1H3a.5.5 0 0 0-.5.5v10a.5.5 0 0 0 .5.5h10a.5.5 0 0 0 .5-.5V8a.5.5 0 0 1 1 0v5a1.5 1.5 0 0 1-1.5 1.5H3z"/>
  <path d="m8.354 10.354 7-7a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0z"/>
</svg>
		:
		<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-app" viewBox="0 0 16 16">
  <path d="M11 2a3 3 0 0 1 3 3v6a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3V5a3 3 0 0 1 3-3h6zM5 1a4 4 0 0 0-4 4v6a4 4 0 0 0 4 4h6a4 4 0 0 0 4-4V5a4 4 0 0 0-4-4H5z"/>
</svg>

		}
		</Button>

		{isActive ?<Button className="btn-danger" onClick={e => updateRefresh(e)}  >Save</Button>
		:
		<Button className= 'btn-primary' onClick={handleClick}>Edit</Button>
		}
		
		


		<Button className='btn-primary'onClick={e => deleteRefresh(e)}>Delete</Button>
		</Card.Body>
		</Card>
		
		</>
	)
}